# pisetup

Scripts for setting up a headless raspberry pi from the Linux command line.

The `pi-setup.sh` script runs on the Pi during the first boot.  The `init-pi.sh`
is run on your local Linux computer to install a Raspberry Pi OS image onto an
SD card for use in a Pi.  Detailed information on what each script does can be
found in [this YouTube video](https://youtu.be/irEU2uJ2BT4).

## Usage

First, you should set the environment variables in `pi-setup.sh` for your Pi's
desired environment.  Next, comment out any function calls you don't want to run
at the bottom of the `pi-setup.sh` script. Download a Raspberry Pi OS image and
save it in the directory with the scripts.  Decompress it with `xz -d`, and run
the `init-pi.sh` script as described:

```
Usage: ./init-pi.sh <os-img> <sd-device> <user-optional> <password-optional>
The username and password are optional. You will be prompted for them if you don't provide them.

Example:   ./init-pi.sh pi-os.img /dev/sdX pi supersecurepassword
```

The script will write the OS image to the SD card, then mount the image on a
temporary directory, copy the custom pi-setup script, and configure the script
to run on boot.

After that is complete, you can eject the SD card, put it in your Pi and boot!
