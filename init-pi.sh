#!/bin/bash
# Script to create a bootable Pi SD card and pre-configure the Pi

if [ $# -lt 2 ]; then
    echo "Usage: $0 <os-img> <sd-device> <user-optional> <password-optional>"
    echo "The username and password are optional. You will be prompted for them if you don't provide them."
    echo -e "\nExample:   $0 pi-os.img /dev/sdX pi supersecurepassword"
    exit 1
fi
OS_IMG=$1
SD_DEVICE=$2
USER=$3
PASSWORD=$4

if [ "${USER}" == "" ]; then
    read -p "Username: " USER
fi

if [ "${PASSWORD}" == "" ]; then
    read -sp "Password: " PASSWORD
    echo ""
fi

# Extra confirmation here for people that just grab the script. If you're confident
# that you're not going to destroy your drives, you can remove this section
read -p "All data on '${SD_DEVICE}' will be destroyed. Type 'yes' if this ok! " AUTH_DD
if [ "${AUTH_DD}" != "yes" ]; then
    echo "Aborting"
    exit 1
fi

# Unmount in case mounted
sudo umount ${SD_DEVICE}1 ${SD_DEVICE}2 || true

# Write the OS image
echo "Writing OS image..."
sudo dd if=${OS_IMG} of=${SD_DEVICE} bs=16M status=progress

# Remount the device so we can write to the boot partition
echo "Mounting SD card..."
sync
TEMP_DIR=$(mktemp -d -t pi.XXXXXXXX)
sudo mount ${SD_DEVICE}1 ${TEMP_DIR}

# Create the user configuration file
echo "Creating user configuration file..."
echo "${USER}:$(echo "${PASSWORD}" | openssl passwd -6 -stdin)" | sudo tee ${TEMP_DIR}/userconf.txt > /dev/null

# Copy our setup script
echo "Copying the first-boot setup script..."
sudo cp pi-setup.sh ${TEMP_DIR}/

# Edit cmdline.txt to enable our setup script to run on boot
echo "Enabling the setup script to run on boot..."
SED="s|^\(.*\)$|\1 systemd.run=/boot/firmware/pi-setup.sh systemd.run_success_action=reboot systemd.unit=kernel-command-line.target|g"
sudo sed -i "${SED}" ${TEMP_DIR}/cmdline.txt

# Unmount and clean up temp directory
echo "Cleaning up..."
sync
sudo umount ${TEMP_DIR}
sudo rm -rf ${TEMP_DIR}

echo "Configuration complete!"
echo "Eject the SD card and use it to boot your Pi!"
