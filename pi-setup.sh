#!/bin/bash
#
# Script to initialize the pi on first setup
# Put script in /boot/firmware and append to cmdline.txt:
# systemd.run=/boot/firmware/pi-setup.sh systemd.run_success_action=reboot systemd.unit=kernel-command-line.target
set +e

# Info section
USER=pi

# SSH section
SSH_PUBKEY="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC4o7nAny6PHwe+46YBplOK5GVBID/3Jkv+yXlMCAj38Q2yO+RQSBCaOo5DcvJDbDBl88ALmLM6h6PvspakY+zhVIZp6K4h07qzSb30mU9zvZuBb16c/xPFITnwaGwdGrXgpkm0XzF1Bh5RfaYCbZOVUDdoprTwLs3nKpS/XjabSpEo412Coxi50HHtKVNaiZ+dWM5mJiO9PVYpE1ui23ZqqQsM4Jhj4IDzuazcmH2B0Cn47rJZsu6N6WEPrRsz3bMWArYe4g8hGb7peW3xWK7zrS4okt0EOXrdDf3Z6EPiWxhgd9/yeVHxKDpk3MDAc02UFv/HjwCQwixmDV7+4HPnYsQgRpMtfIgj/KVgAVTlnBzDoOO8Qu77kNXlW4FCsRLwSCpZZ40+HUm7yicW2xtsT5jEQ4iNANDcwcVme1AJuLmi1oypQOl/VFnfV+whsJONnTmPq2VOJK3E8fKxfTsaw+TDeoQhUb4stqfehe8nqfSyZjvNtQhhz0kqh/Rdd5U= carpie@thedoctor"

# Wireless information
COUNTRY_CODE="us"
SSID="tardis"
# PSK can be the passphrase or actual key
#PSK="itsbiggerontheinside"
PSK="9901be38595fa9d0c64009cfb60f0ed7d5aa3dd2af232a8b978120dcedfd764b"

# Static IP information
IP_ADDR_AND_MASK=192.168.0.5/24
ROUTERS=192.168.0.1
DNS=192.168.0.1

# General config information
HOSTNAME=example-pi
TIMEZONE="US/Eastern"
KB_LAYOUT="us"

# Go to the bottom of the file and uncomment the options you want to call


# Enable ssh
function enable_ssh {
    systemctl enable ssh
}

function add_ssh_key {
    install -o "${USER}" -m 700 -d "/home/${USER}/.ssh"
    install -o "${USER}" -m 600 <(echo ${SSH_PUBKEY}) /home/${USER}/.ssh/authorized_keys
}

function disable_password_auth {
    echo "PasswordAuthentication no" >>/etc/ssh/sshd_config
}

function create_nm_wifi_config() {
    if [ "$1" == "static" ]; then
        cat >"/etc/NetworkManager/system-connections/${SSID}.nmconnection" << NM_EOF_STATIC
[connection]
id=${SSID}
uuid=$(cat /proc/sys/kernel/random/uuid)
type=wifi

[wifi]
mode=infrastructure
ssid=${SSID}

[wifi-security]
key-mgmt=wpa-psk
psk=${PSK}

[ipv4]
address1=${IP_ADDR_AND_MASK},${ROUTERS}
dns=${DNS};
method=manual

[ipv6]
addr-gen-mode=default
method=auto

[proxy]
NM_EOF_STATIC
    else
        cat >"/etc/NetworkManager/system-connections/${SSID}.nmconnection" << NM_EOF
[connection]
id=${SSID}
uuid=$(cat /proc/sys/kernel/random/uuid)
type=wifi

[wifi]
mode=infrastructure
ssid=${SSID}

[wifi-security]
key-mgmt=wpa-psk
psk=${PSK}

[ipv4]
method=auto

[ipv6]
addr-gen-mode=default
method=auto

[proxy]
NM_EOF
    fi
    chmod 600 "/etc/NetworkManager/system-connections/${SSID}.nmconnection"
}

function create_nm_eth_config() {
    if [ "$1" == "static" ]; then
        cat >"/etc/NetworkManager/system-connections/Ethernet.nmconnection" << NM_EOF_ETH_STATIC
[connection]
id=Ethernet
uuid=$(cat /proc/sys/kernel/random/uuid)
type=ethernet

[ethernet]

[ipv4]
address1=${IP_ADDR_AND_MASK},${ROUTERS}
dns=${DNS};
method=manual

[ipv6]
addr-gen-mode=default
method=auto

[proxy]
NM_EOF_ETH_STATIC
    else
        cat >"/etc/NetworkManager/system-connections/Ethernet.nmconnection" << NM_EOF_ETH
[connection]
id=Ethernet
uuid=$(cat /proc/sys/kernel/random/uuid)
type=ethernet

[ethernet]

[ipv4]
address1=${IP_ADDR_AND_MASK},${ROUTERS}
dns=${DNS};
method=manual

[ipv6]
addr-gen-mode=default
method=auto

[proxy]
NM_EOF_ETH
    fi
    chmod 600 "/etc/NetworkManager/system-connections/Ethernet.nmconnection"
}

# Set wireless configuration
function enable_wifi {
    # Create a network manager wifi connections
    create_nm_wifi_config
    # Unblock the wifi radio
    rfkill unblock wifi
    for filename in /var/lib/systemd/rfkill/*:wlan ; do
      echo 0 > $filename
    done
}

function set_static_wired_address {
    create_nm_eth_config "static"
}

function set_static_wireless_address {
    create_nm_wifi_config "static"
}

# Set hostname
function set_hostname {
    CURRENT_HOSTNAME=`cat /etc/hostname | tr -d " \t\n\r"`
    echo ${HOSTNAME} > /etc/hostname
    sed -i "s/127.0.1.1.*${CURRENT_HOSTNAME}/127.0.1.1\t${HOSTNAME}/g" /etc/hosts
}

# Set timezone
function set_timezone {
    rm -f /etc/localtime
    echo "${TIMEZONE}" >/etc/timezone
    dpkg-reconfigure -f noninteractive tzdata
}

# Set keyboard layout
function set_keyboard_layout {
    cat >/etc/default/keyboard << KB_EOF
XKBMODEL="pc105"
XKBLAYOUT="${KB_LAYOUT}"
XKBVARIANT=""
XKBOPTIONS=""
KB_EOF
    dpkg-reconfigure -f noninteractive keyboard-configuration
}

# Uncomment the pieces you want
enable_ssh
add_ssh_key
disable_password_auth
enable_wifi
set_static_wired_address
set_static_wireless_address
set_hostname
set_timezone
set_keyboard_layout

# Remove ourselves from running at boot
rm -f /boot/firmware/pi-setup.sh
sed -i 's| systemd.run.*||g' /boot/firmware/cmdline.txt
exit 0
